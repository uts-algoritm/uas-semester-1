/*
Saya mengerjakan soal nomor 3
Rafly Maulannasir
301230031
IF 1A
*/


#include <iostream>
using namespace std;

#define MAX_MAHASISWA 100 // Batas maksimal jumlah mahasiswa

struct Mahasiswa {
    double absen, quiz, uts, uas, tugas, nilai;
    char Huruf_Mutu;
};

int main() {
    int jumlah_mahasiswa, i = 0;
    Mahasiswa mahasiswa[MAX_MAHASISWA];

    cout << "Masukkan jumlah mahasiswa: ";
    cin >> jumlah_mahasiswa;

    while (i < jumlah_mahasiswa) {
        cout << "Masukkan nilai mahasiswa ke-" << i + 1 << ":" << endl;
        cout << "Absen: ";
        cin >> mahasiswa[i].absen;
        cout << "Quiz: ";
        cin >> mahasiswa[i].quiz;
        cout << "UTS: ";
        cin >> mahasiswa[i].uts;
        cout << "UAS: ";
        cin >> mahasiswa[i].uas;
        cout << "Tugas: ";
        cin >> mahasiswa[i].tugas;

        mahasiswa[i].nilai = ((0.1 * mahasiswa[i].absen) + (0.2 * mahasiswa[i].tugas) +
                              (0.3 * mahasiswa[i].quiz) + (0.4 * mahasiswa[i].uts) +
                              (0.5 * mahasiswa[i].uas)) / 2;

        if (mahasiswa[i].nilai > 85 && mahasiswa[i].nilai <= 100)
            mahasiswa[i].Huruf_Mutu = 'A';
        else if (mahasiswa[i].nilai > 70 && mahasiswa[i].nilai <= 85)
            mahasiswa[i].Huruf_Mutu = 'B';
        else if (mahasiswa[i].nilai > 55 && mahasiswa[i].nilai <= 70)
            mahasiswa[i].Huruf_Mutu = 'C';
        else if (mahasiswa[i].nilai > 40 && mahasiswa[i].nilai <= 55)
            mahasiswa[i].Huruf_Mutu = 'D';
        else if (mahasiswa[i].nilai >= 0 && mahasiswa[i].nilai <= 40)
            mahasiswa[i].Huruf_Mutu = 'E';

        cout << "Nilai Mahasiswa ke-" << i + 1 << ": " << mahasiswa[i].nilai << endl;
        cout << "Huruf Mutu Mahasiswa ke-" << i + 1 << ": " << mahasiswa[i].Huruf_Mutu << endl;

        i++;
    }

    return 0;
}
